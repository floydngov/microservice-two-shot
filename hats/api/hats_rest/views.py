
from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Hat, LocationVO
from common.json import ModelEncoder

# Create your views here.

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
        "section_number",
        "shelf_number",
    ]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = [  
        "id",    
        "fabric",
        "style_name",
        "color",
        "picture_url",
    ]
    def get_extra_data(self, o):
        return {"location": o.location.closet_name}

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        
        "fabric", 
        "style_name",
        "color",
        "picture_url",
        
    ]
    

@require_http_methods(["GET", "POST"])
def api_list_hats(request, id =None):
    if request.method == "GET":
        if id is not None:
            hats = Hat.objects.filter(location=id)
        else:
            hats = Hat.objects.all()
        return JsonResponse(
            {"hats" : hats},
            encoder=HatListEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            location_href = content["location"]
            location = LocationVO.objects.get(import_href=location_href)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid locations id"},
                status=400,
            )
        hats = Hat.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatDetailEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def api_show_hats(request, id):
        count, _ = Hat.objects.get(id=id).delete()
        return JsonResponse({"deleted": count > 0})
    