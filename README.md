# Wardrobify

Team:

<<<<<<< HEAD
* Person 1 - Which microservice? israel is doing hats
=======
* Person 1 - Floyd Ngov Shoes
>>>>>>> main
* Person 2 - Which microservice?

## Design

## Shoes microservice

Created class models for Shoes and Bin Value objects that will enable use of existing bin model found in the database. For models I ensured that there is a relationship between shoes and bins where a bin is required to create a shoe with a ForeignKey specification on the shoe model class.

Next I created additional classes for encoders which would be used to specifcy different properties of shoes or bins within my view functions. For my view functions, I was able to create functions that will handle HTTP requests such as GET, POST, and DELETE. This would enable users to create new shoes and bins while also having the ability to delete them. Upon the creation of these view functions, I made sure to include specifc URL paths that would utilize these functions given a specific HTTP request.

Upon completion of these functions, I was able to test them through insomnia utilizing the specific url addresses where bins are stored. Along with this step I finished my shoes poller which extracts data from the database where in this instance was bins. Since a bin is required to create a shoe, users must specify which bin the shoe they are creating would be stored in.

When creating the frontend component of our application, I started by creating files for Listing shoes and a form for creating shoes. My ShoeList file includes a Shoelist function that will enable deletion of shoes on the front-end of the application. This deleteShoe function is utilized through a delete button found on the shoelist next to each corresponding shoe. Along with the ShoeList function, I provided the appropriate JSX component of REACT to populate the page with a user friendly UI.

Next I created a shoeForm file which is similiar to my ShoeList file where instead it utilizes useState and useEffect in a function to manage and handle event changes on the form. This would enable user inputs to be tracked and recorded into the application. Along with this function, I had to ensure that a working dropdown menu would be enabled to allow users to choose the appropriate existing bins to store their newly created shoe. I also created the JSX for the form which would be the user's input method for creating new shoes.

Finally I was able to pass these functions into another function: Apps. This function sits on our App.js file which is used to create a one page application. I utilized react's different Route functions which allow me to specify the different paths of the application such as when a user wants to view all shoes, create a shoe, or delete a shoe. 

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
