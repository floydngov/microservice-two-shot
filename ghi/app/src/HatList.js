import React from "react";
import { Link } from "react-router-dom";

function HatList({ hats, getHats }) {
    const deleteHat = async (id) => {  //method to delete hats
        fetch(`http://localhost:8090/api/hats/${id}/`, { //fetches this endpoint gets by id
            method: "delete",
        })
            .then(() => {           // after delete 
                return getHats()  // fires function to get data & bring back updated state
            })
            .catch(console.log)
    }
    if (hats === undefined) {
        return null
    }

    return (
        <>
            <table className="table table-striped align-middle mt-5">
                <thead>
                    <tr>
                        <th>Fabric</th>
                        <th>Style</th>
                        <th>Picture</th>
                        <th>Color</th>
                        <th>Location</th>
                        <th>Remove</th>
                    </tr>
                </thead>
                <tbody>
                    {hats.map((hat) => {
                        return (
                            <tr key={hat.id}>
                                <td>{hat.fabric}</td>
                                <td>{hat.style_name}</td>
                                <td>{<img src={hat.picture_url} alt="" width="100px" height="100px" />} </td>
                                <td>{hat.color}</td>
                                <td>{hat.location}</td>
                                <td>
                                    <button type="button" value={hat.id} onClick={() => deleteHat(hat.id)}>Delete</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
            <Link to={"/new"}>Create hat</Link>
        </>
    );
}

export default HatList;
