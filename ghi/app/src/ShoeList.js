import React from 'react';
import { Link } from 'react-router-dom';

function ShoeList(props) {

    async function deleteShoe(id) {
        const shoeURL = `http://localhost:8080/api/shoes/${id}`;
        const fetchConfig = {
            method: "delete",
        };
        const response = await fetch (shoeURL, fetchConfig);
        if (response.ok) {
            const deleted = await response.json();
            console.log(deleted);
            props.fetchShoes();

        }
    }

    return (
        <><table className="table table-striped">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>Model Name</th>
                    <th>Color</th>
                    <th>Picture</th>
                    <th>Bin</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                {props.shoes.map(shoe => {
                    return (
                        <tr key={shoe.id}>
                            <td>{shoe.manufacturer}</td>
                            <td>{shoe.name}</td>
                            <td>{shoe.color}</td>
                            <td>
                                <img
                                    src={shoe.picture_URL}
                                    alt=""
                                    width="75px"
                                    height="76px" />
                            </td>
                            <td>{shoe.bin}</td>
                            <td>
                                <button className="btn btn-outline-danger" onClick={() => deleteShoe(shoe.id)}>Delete</button>
                            </td>
                        </tr>
                    );
                })}
            </tbody>
        </table><Link to={"/shoes/new"}>Create Shoe</Link></>
      );
    }

export default ShoeList;
