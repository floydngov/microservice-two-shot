
import React, {useEffect, useState} from 'react'
function ShoeForm(props) {

    const [bins, setBins] = useState([]);
    const [manufacturer, setManufacturer] = useState('');
    const [name, setName] = useState('');
    const [color, setColor] = useState('');
    const [picture_URL, setPicture_URL] = useState('');
    const [bin, setBin] = useState('');
    const handleChangeManufacturer = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }
    const handleChangeName = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleChangeColor= (event) => {
        const value = event.target.value;
        setColor(value);
    }
    const handleChangePicture= (event) => {
        const value = event.target.value;
        setPicture_URL(value);
    }
    const handleChangeBin= (event) => {
        const value = event.target.value;
        setBin(value);
    }
    const handleSubmit = async (event) => {
      event.preventDefault();
      const data = {};
      data.manufacturer = manufacturer;
      data.name = name;
      data.color = color;
      data.picture_URL = picture_URL;
      data.bin = bin;

      const shoeUrl = 'http://localhost:8080/api/shoes/';
      const fetchConfig = {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const shoesResponse = await fetch(shoeUrl, fetchConfig);
      console.log(shoesResponse);
      if (shoesResponse.ok) {
        const newShoe = await shoesResponse.json();
        console.log(newShoe);
        setManufacturer("");
        setName("");
        setColor("");
        setPicture_URL("");
        setBin("");
        props.fetchShoes();



      }
    }
    const fetchData = async () => {
        const binUrl = "http://localhost:8100/api/bins/";
        const binResponse = await fetch(binUrl);
        if (binResponse.ok){
            const data = await binResponse.json();
            console.log(data);
            setBins(data.bins);
        }
    }
        useEffect(() => {
            fetchData();
        }, []);
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new shoe</h1>
                    <form onSubmit={handleSubmit} id="create-shoe-form">
                        <div className="form-floating mb-3">
                        <input onChange={handleChangeManufacturer} value={manufacturer} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control"/>
                        <label htmlFor="manufacturer">Manufacturer</label>
                        </div>
                        <div className="form-floating mb-3">
                        <input onChange={handleChangeName} value={name} placeholder="Model Name" required type="text" name="model_name" id="model_name" className="form-control"/>
                        <label htmlFor="model_name">Model Name</label>
                        </div>
                        <div className="form-floating mb-3">
                        <input onChange={handleChangeColor} value={color} placeholder="Color" required type="text" name="color" id="color" className="form-control"/>
                        <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                        <input onChange={handleChangePicture} value={picture_URL} placeholder="Picture Url" required type="text" name="picture_url" id="picture_url" className="form-control"/>
                        <label htmlFor="picture_url">Picture Url</label>
                        </div>
                        <div className="mb-3">
                        <select onChange={handleChangeBin} value={bin} required name="bin" id="bin" className="form-select">
                            <option  value="">Choose a bin</option>
                            {bins.map(bin => {
                                return (
                                    <option key={bin.id} value={bin.href}>
                                    {bin.closet_name}
                                    </option>
                                );
                            })}
                        </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
         </div>
    )
}
export default ShoeForm;

