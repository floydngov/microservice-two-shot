import MainPage from './MainPage';
import Nav from './Nav';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import HatList from './HatList';
import HatForm from './HatForm';
import { useState, useEffect } from "react";
import ShoeList from './ShoeList';
import ShoeForm from './ShoeForm';

function App(props) {
  const [hats, setHats] = useState([])
  const [shoes, setShoes] = useState([])

  const getHats = async () => {
    const url = 'http://localhost:8090/api/hats/'
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      const hats = data.hats
      setHats(hats)
    }
  }
  
  console.log(shoes);
  const fetchShoes = async () => {
  const shoesUrl = "http://localhost:8080/api/shoes/";
  const shoesResponse = await fetch(shoesUrl);
  if (shoesResponse.ok){
      const data = await shoesResponse.json();
      setShoes(data.shoes);
    }
  }
  useEffect(() => {
    getHats();
    fetchShoes();
    }, []);
    if (shoes === undefined){
      return null;
    }

  

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/hats" element={<HatList hats={hats} getHats={getHats} />} />
          <Route path="/new" element={<HatForm getHats={getHats} /> } />
          <Route path="shoes">
              <Route index element={<ShoeList shoes={shoes} fetchShoes={fetchShoes} />} />
              <Route path="new" element={<ShoeForm bins={shoes} fetchShoes={fetchShoes}/>} />
            </Route>
        </Routes>
      </div>
    </BrowserRouter >
  );
}

export default App;




    
    
    
